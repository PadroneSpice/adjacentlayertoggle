# Adjacent Layer Visibility Toggle Script
# by Sean Castillo with feedback from Grum999 on krita-artists.org
# 3 + 4 May 2020
# Task: Toggles the visibility of layerA and layerB,
# such that layerA is the currently selected layer
# and layerB is the layer directly below.
#
# If both layers are turned off, then layerA is turned on.
# If both layers are turned on, then layerB is turned off.
#
# Finding an adjacent layer requires looping through
# the layers of the parent because layers don't
# have pointers to their siblings.

from krita import *

doc    = Krita.instance().activeDocument()
if not doc is None:
    layerA = doc.activeNode()

    if not layerA is None:
        layerB      = None
        layerParent = layerA.parentNode()

    for currChild in layerParent.childNodes():
        if currChild == layerA:
            break
        layerB = currChild

    if not layerB is None:
        if layerA.visible() != layerB.visible():
            layerA.setVisible(not layerA.visible())
            layerB.setVisible(not layerB.visible())
        elif layerA.visible() == True:
            layerB.setVisible(False)
        else:
            layerA.setVisible(True)
        doc.refreshProjection()