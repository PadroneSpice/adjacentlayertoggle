# README #

Adjacent Layer Toggle Script
for Krita
by Sean Castillo
3 + 4 May 2020

Toggles the visibility of two adjacent layers:
Layer A: The currently selected layer
Layer B: The layer directly below

If both layers are invisible, Layer A is turned on.
If both layers are visible, Layer B is turned off.

This is useful in comparing two layers that are different versions
of the same function (for example, two versions of an ink layer).

It determines Layer B by looping through all the layers of the group
because layers have pointers to parents and to children but not to siblings.